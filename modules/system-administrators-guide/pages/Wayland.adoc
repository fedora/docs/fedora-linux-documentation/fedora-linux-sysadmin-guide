:hardbreaks:
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Wayland]]
= The Wayland Protocol

Wayland is a display server protocol which was (at the time of writing) introduced as the default in GNOME. It is said that Wayland will eventually replace X11 as the default display server on Linux and many distributions have begun implementation of Wayland. Wayland is a more modern protocol and has a smaller code base currently. Wayland is still under development, and there are still applications and behaviours that don't work as expected, you may find that some applications have not been updated to work properly in Wayland and currently the only way these applications will run is using Xorg instead of Wayland. This includes some legacy system applications and games.

Wayland is enabled by default in the GNOME Desktop. You can choose to run GNOME in X11 by choosing the Gnome on xorg option in the session chooser on the login screen. Currently KDE still uses X11 and although there is a plasma-wayland session available, it is not considered stable or bugfree at this time.

== Determining whether you are using Wayland
One way to determine if you're running in Wayland, is to check the value of the variable $WAYLAND_DISPLAY. To do this type:

[source,bash]
----
$ echo $WAYLAND_DISPLAY
wayland-0
----

If you are not running under Wayland the variable will not contain any values. You can also use loginctl to show you what type of session is running:

[source,bash]
----
$ loginctl show-session <YOUR_SESSION_NUMBER> -p Type
----

To determine your session number, simply typing `loginctl` should provide your session details.

There is also a legacy X11 server provided with Wayland for compatibility purposes. To determine what applications are running in this mode, you can run the following command:

[source,bash]
----
$ xlsclients
----

There is also the `lg` (looking glass) tool in GNOME that will allow you to determine which protocol a specific window is using. To do this, you run the application by typing `lg` in the run dialog or at the command line, select "`Windows`" in the upper right corner of the tool, and click on the application name (or open window) you want to know about. If the window is running in wayland it will say "`MetaWindowWayland`" and if it is running in X11 it will say "`MetaWindowX11`".

== Additional Resources
To find out more about Wayland, please see the following website:

https://wayland.freedesktop.org/

If you need to determine if an issue you are experiencing is related to wayland, see the Fedora wiki at the link below:

https://fedoraproject.org/wiki/How_to_debug_Wayland_problems
